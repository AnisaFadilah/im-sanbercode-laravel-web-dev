<?php
require_once ('animal.php');
require_once ('ape.php');
require_once ('frog.php');

$sheep = new animal("shaun");
echo "Name: $sheep->name<br>"; // "shaun"
echo "Legs: $sheep->legs<br>"; // 4
echo "cold blooded: $sheep->cold_blooded<br>"; // "no"

echo"<br><br>";

$kodok = new frog("buduk");
echo "Name: $kodok->name<br>"; 
echo "Legs: $kodok->legs<br>"; 
echo "cold blooded: $kodok->cold_blooded<br>"; 
echo "Jump:";
echo $kodok->jump();

echo"<br><br>";

$sungokong = new ape("kera sakti");
echo "Name: $sungokong->name<br>"; 
echo "Legs: $sungokong->legs<br>"; 
echo "cold blooded:$sungokong->cold_blooded<br>"; 
echo "Yell:";
echo $sungokong->yell();
?>
