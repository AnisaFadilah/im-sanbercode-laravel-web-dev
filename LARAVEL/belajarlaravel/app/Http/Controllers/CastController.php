<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('/kategori/tambah');
    }

    public function store(Request $request)
    {
        //untuk membuat aturan agar data dapat tersimpan jika data tertentu telah diisi
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);  

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);
        
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('kategori.tampil', ['cast' => $cast]);
    }
    
    public function show($id){
        $peran = DB::table('cast')->find($id);
        return view('kategori.detail', ['peran'=>$peran]);
    }

    public function edit($id){
        $peran = DB::table('cast')->find($id); //mengambil berdasarkan id
        return view('kategori.edit', ['peran'=>$peran]);
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
            DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio'],
                ]
                );
                return redirect('/cast');
         
    }

    public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }

}
