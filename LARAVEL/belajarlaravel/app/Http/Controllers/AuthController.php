<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('page.form');
    }

    public function kirim(Request $request)
    {
        //dd($request->all());
        $namaDepan = $request['firstname'];
        $namaBelakang = $request['lastname'];
        return view('page.home', ['namaDepan'=>$namaDepan, 'namaBelakang'=>$namaBelakang]);
    }
}
