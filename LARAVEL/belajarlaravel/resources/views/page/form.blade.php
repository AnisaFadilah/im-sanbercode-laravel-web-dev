@extends('layout.master')
@section('title')
<h1>Buat Account Baru!</h1>   
@endsection 

@section('sub-title')
<h3>Sign Up Form</h3>  
@endsection   

@section('content')
<form action="/kirim" method="post">
    @csrf
    <label>First name:</label><br><br>
    <input type="text" name="firstname"><br><br>
    <label>Last name:</label><br><br>
    <input type="text" name="lastname"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br><br>
    <label>Nationality:</label><br><br>
    <select>
        <option>Indonesia</option>
        <option>Malaysia</option>
        <option>Rusia</option>
        <option>Korea Selatan</option>
        <option>Jepang</option>
        <option>Finlandia</option>
    </select><br><br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox" name="language">Bahasa Indonesia<br>
    <input type="checkbox" name="language">English<br>
    <input type="checkbox" name="language">Other<br><br>
    <label>Bio:</label><br><br>
    <textarea name="bio" rows="10" cols="30"></textarea><br>
    <input type="submit" value="sign up">
    </form>

 
@endsection 
    