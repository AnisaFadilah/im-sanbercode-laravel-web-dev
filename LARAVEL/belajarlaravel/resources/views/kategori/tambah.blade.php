@extends('layout.master')
@section('title')
    Halaman Tambah Cast
@endsection

@section('sub-title')
    Cast
@endsection

@section('content')
<form action='/cast' method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label for="exampleInputPassword1">Umur</label>
      <input type="number" name="umur" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <label>Bio</label>
    <textarea class="form-control" name="bio" rows="3"></textarea>   
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection