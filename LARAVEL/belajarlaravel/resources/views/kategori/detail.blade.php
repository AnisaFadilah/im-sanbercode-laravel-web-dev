@extends('layout.master')
@section('title')
    Halaman Detail Cast
@endsection

@section('sub-title')
    Cast
@endsection

@section('content')
<h1>{{$peran->nama}}</h1>
<p>{{$peran->umur}}</p>
<p>{{$peran->bio}}</p>
<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection