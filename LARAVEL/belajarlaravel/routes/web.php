<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'dashboard']);

Route::get('/form', [AuthController::class, 'daftar']);

Route::post('/kirim', [AuthController::class, 'kirim']);

Route::get('/data-table', function(){
  return view('page.data-table');
});

Route::get('/table', function(){
    return view('page.table');
  });

  //CRUD

  //CREATE
  //Route mengarah ke form tambah cast
  Route::get('/cast/create',[CastController::class,'create']);

  //Route untuk menyimpan inputan kedalam database table cast
  Route::post('/cast',[CastController::class,'store']);

  //Read
  //Route mengarah ke halaman tampil semua data di table cast
  Route::get('/cast',[CastController::class,'index']);

  //Route detail cast berdasarkan id
  Route::get('/cast/{id}',[CastController::class,'show']);

  //Update data
  //Route mengarah ke form update cast
  Route::get('/cast/{id}/edit',[CastController::class,'edit']);

  //Route untuk edit data berdasarkan id cast
  Route::put('/cast/{id}',[CastController::class,'update']);

  //Delete data
  Route::delete('/cast/{id}', [CastController::class,'destroy']);


